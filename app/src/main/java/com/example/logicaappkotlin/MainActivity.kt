package com.example.logicaappkotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        subButton.setOnClickListener {

            submit()
        }

    }
    private fun submit(){
        var answer1 = answer1.text.toString()
        var answer2 = answer2.text.toString()
        var answer3 = answer3.text.toString()
        var answer4 = answer4.text.toString()
        var score =0;
        if(answer1 =="t"){
        score++;
        }
        if(answer2 =="f"){
            score++;
        }
        if(answer3 =="f"){
            score++;
        }
        if(answer4 =="f"){
            score++;
        }
        var message = "Your score is: "+score.toString()
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

    }
}
